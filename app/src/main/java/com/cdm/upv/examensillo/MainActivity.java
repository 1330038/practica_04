package com.cdm.upv.examensillo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE="com.cdm.upv.examensillo.MESSAGE";
    TabHost tabH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        tabH= (TabHost)findViewById(R.id.tabHost);
        tabH.setup();

        TabHost.TabSpec tab1 = tabH.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabH.newTabSpec("tab2");
        TabHost.TabSpec tab3 = tabH.newTabSpec("tab3");

        tab1.setIndicator("Temp ºF-ºC");
        tab1.setContent(R.id.tab_temperatura);

        tab2.setIndicator("Area");
        tab2.setContent(R.id.tab_area);

        tab3.setIndicator("Distancia");
        tab3.setContent(R.id.tab_distancia);

        tabH.addTab(tab1);
        tabH.addTab(tab2);
        tabH.addTab(tab3);
    }

    public void enviarTemperatura(View view){
        EditText ed = (EditText)findViewById(R.id.etxt_grados);
        Double res = ((Double.valueOf(ed.getText().toString())-32)/1.8);
        Intent intent = new Intent(this,Resultados.class);
        intent.putExtra(EXTRA_MESSAGE,res.toString());
        startActivity(intent);
    }

    public void enviarArea(View view){
        EditText ed = (EditText)findViewById(R.id.etxt_area);
        Double res = (Double.valueOf(ed.getText().toString()))*(Double.valueOf(ed.getText().toString()));
        Intent intent = new Intent(this,Resultados.class);
        intent.putExtra(EXTRA_MESSAGE,res.toString());
        startActivity(intent);
    }

    public void enviarMillas(View view){
        EditText ed = (EditText)findViewById(R.id.etxt_distancia);
        Double res = (Double.valueOf(ed.getText().toString()))*1.62;
        Intent intent = new Intent(this,Resultados.class);
        intent.putExtra(EXTRA_MESSAGE,res.toString());
        startActivity(intent);
    }
}
